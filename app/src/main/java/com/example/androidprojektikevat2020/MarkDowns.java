package com.example.androidprojektikevat2020;

import com.example.androidprojektikevat2020.Model.MarkDown;

import java.util.ArrayList;
import java.util.List;

public class MarkDowns {

  private static List<MarkDown> markDowns = new ArrayList<>();


    public MarkDowns() {

    }

    public void addMarkDown(MarkDown m) {

        markDowns.add(m);

    }

    public static List<MarkDown> getMarkDowns() {

        return markDowns;
    }

}
