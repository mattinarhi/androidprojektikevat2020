package com.example.androidprojektikevat2020;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.androidprojektikevat2020.Model.MarkDown;

import java.util.List;

public class MarkDownListAdapter extends RecyclerView.Adapter<MarkDownListAdapter.MarkDownViewHolder> {

    class MarkDownViewHolder extends RecyclerView.ViewHolder {

        private final TextView dateView;
        private final TextView stationView;
        private final TextView fuelView;
        private final TextView litersView;
        private final TextView priceView;
        private final TextView consumptionView;
        private final TextView drivenView;

       private MarkDownViewHolder(View itemView) {

           super(itemView);

           dateView = itemView.findViewById(R.id.date);
           stationView = itemView.findViewById(R.id.station);
           fuelView = itemView.findViewById(R.id.fuel);
           litersView = itemView.findViewById(R.id.liters);
           priceView = itemView.findViewById(R.id.price);
           consumptionView = itemView.findViewById(R.id.consumption);
           drivenView = itemView.findViewById(R.id.driven);



        }

    }

    private final LayoutInflater mInflater;
    private List<MarkDown> markdowns; // Cached copy of words

    MarkDownListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public MarkDownViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_markdown_item, parent, false);
        return new MarkDownViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MarkDownViewHolder holder, int position) {
        if (markdowns != null) {
            MarkDown current = markdowns.get(position);


            holder.dateView.setText("Pvm: " + current.getDate());
            holder.stationView.setText("Asema: " + current.getStation());
            holder.fuelView.setText("Polttoaine: " + current.getFuel());
            holder.litersView.setText("Litrat: " + current.getAmount().toString());
            holder.priceView.setText("Hinta: " + current.getPrice().toString() + " €/l");
            holder.consumptionView.setText("Kulutus: " + current.getConsumption().toString() + " l/100 km");
            holder.drivenView.setText("Ajettu: " + current.getDriven().toString() + " km");

        } else {
            // Covers the case of data not being ready yet.
            holder.dateView.setText("Ei merkintöjä");

        }
    }

    public String getDate(View itemView) {

        return ((TextView) itemView.findViewById(R.id.date)).getText().toString();

    }

    public String getStation(View itemView) {

        return((TextView) itemView.findViewById(R.id.station)).getText().toString();
    }

    public String getFuel(View itemView) {

        return ((TextView) itemView.findViewById(R.id.fuel)).getText().toString();
    }

    public String getLiters(View itemView) {

        return ((TextView) itemView.findViewById(R.id.liters)).getText().toString();
    }

    public String getConsumption(View itemView) {

        return ((TextView) itemView.findViewById(R.id.consumption)).getText().toString();
    }

    public String getDriven(View itemView) {

        return ((TextView) itemView.findViewById(R.id.driven)).getText().toString();
    }

    public String getPrice(View itemView) {

        return ((TextView) itemView.findViewById(R.id.price)).getText().toString();
    }

    public void setMarkdowns(List<MarkDown> markdowns) {

        this.markdowns = markdowns;
        notifyDataSetChanged();
    }

    public int getItemCount() {

        if(markdowns != null) {

            return markdowns.size();
        }

        else {
            return 0;
        }
    }

}
