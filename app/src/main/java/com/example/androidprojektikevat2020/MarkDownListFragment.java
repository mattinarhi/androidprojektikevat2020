package com.example.androidprojektikevat2020;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidprojektikevat2020.Model.MarkDown;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MarkDownListFragment extends Fragment {

    RecyclerView recyclerView;
    private MarkdownViewModel mViewModel;
    private MarkDownListAdapter adapter;

    private FloatingActionButton fab;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        adapter = new MarkDownListAdapter(inflater.getContext());
        mViewModel = ViewModelProviders.of(this).get(MarkdownViewModel.class);



            mViewModel.getAllMarkdowns().observe(getViewLifecycleOwner(), new Observer<List<MarkDown>>() {
                @Override
                public void onChanged(List<MarkDown> markDowns) {
                    adapter.setMarkdowns(markDowns);
                }
            });


        View view = inflater.inflate(R.layout.list_fragment, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);


        fab = (FloatingActionButton)view.findViewById(R.id.fab1);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                alertDialogBuilder.setMessage("Kaikki merkinnät poistetaan.");
                        alertDialogBuilder.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        mViewModel.deleteAll();
                                    }
                                });

                alertDialogBuilder.setNegativeButton("Peruuta",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        return;

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }




        });

        return view;

}

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
}
