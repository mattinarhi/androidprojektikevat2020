package com.example.androidprojektikevat2020;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.androidprojektikevat2020.Model.MarkDown;

import java.util.List;

@Dao
public interface MarkdownDAO {

    @Insert
    void insert(MarkDown markDown);

    @Query("DELETE FROM markdowns")
    void deleteAll();

    @Query("SELECT * from markdowns ORDER BY markdownID DESC")
    LiveData<List<MarkDown>> getAllMarkdowns();
}
