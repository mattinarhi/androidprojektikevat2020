package com.example.androidprojektikevat2020;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.androidprojektikevat2020.Model.MarkDown;

import java.util.List;

public class MarkDownRepository {

    private MarkdownDAO mDao;
    private LiveData<List<MarkDown>> mAllMarkDowns;

    MarkDownRepository(Application application) {
        MarkdownDatabase db = MarkdownDatabase.getDatabase(application);
        mDao = db.markdownDAO();
        mAllMarkDowns = mDao.getAllMarkdowns();
    }


    LiveData<List<MarkDown>> getAllCurrencies() {
        return mAllMarkDowns;
    }

    public void deleteAll() {
        new deleteAsyncTask(mDao).execute();
    }

    private static class deleteAsyncTask extends AsyncTask<Void, Void, Void> {

        private MarkdownDAO mAsyncTaskDao;

        deleteAsyncTask(MarkdownDAO dao) {
            mAsyncTaskDao = dao;
        }

        protected Void doInBackground(final Void... params) {
            mAsyncTaskDao.deleteAll();
            return null;

        }
}

    public void insert (MarkDown m) {
        new insertAsyncTask(mDao).execute(m);
    }

    private static class insertAsyncTask extends AsyncTask<MarkDown, Void, Void> {

        private MarkdownDAO mAsyncTaskDao;

        insertAsyncTask(MarkdownDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MarkDown... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}

