package com.example.androidprojektikevat2020;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.androidprojektikevat2020.Model.MarkDown;

import java.util.List;

@Database(entities = {MarkDown.class}, version = 1)
public abstract class MarkdownDatabase extends RoomDatabase {

    public abstract MarkdownDAO markdownDAO();

    private static MarkdownDatabase INSTANCE;

    public static MarkdownDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MarkdownDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MarkdownDatabase.class, "currency_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final MarkdownDAO mDao;

        PopulateDbAsync(MarkdownDatabase db) {
            mDao = db.markdownDAO();
        }

        @Override
        protected Void doInBackground(final Void... params) {


            List<MarkDown> markDowns = MarkDowns.getMarkDowns();






            for(MarkDown m : markDowns)


                mDao.insert(m);

            return null;
        }
    }
}


