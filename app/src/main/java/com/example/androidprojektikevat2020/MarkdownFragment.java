package com.example.androidprojektikevat2020;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;

import com.example.androidprojektikevat2020.Model.MarkDown;

public class MarkdownFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private Spinner spinner2;

    private Button fuelButton;

    private RadioGroup radioGroup;

    private RadioButton radioButton;

    private Button okButton;

    private EditText date;

    private EditText liters;

    private EditText price;

    private EditText driven;

    private EditText consumption;

    private double amount1;

    private double prices1;

    private double kilometers1;

    private double mpg1;





    private String station;

    private static final String[] fuels = {"95E10", "98E5", "Diesel", "RE85", "Maakaasu"};
    private static final String [] stations = {"Shell", "Teboil", "Neste", "ABC", "ST1", "SEO"};

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        View v = inflater.inflate(R.layout.markdown_fragment, container, false);

        spinner = (Spinner)v.findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item,stations);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        radioGroup = (RadioGroup)v.findViewById(R.id.fuels);


        date = (EditText)v.findViewById(R.id.input1);

        liters = (EditText)v.findViewById(R.id.input2);

        price = (EditText)v.findViewById(R.id.input3);

        driven = (EditText)v.findViewById(R.id.input4);

        consumption = (EditText)v.findViewById(R.id.input5);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId != -1) {
                    Toast.makeText(getActivity(), rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });




   //     addListenerOnButton();





        // Inflate the layout for this fragment


        return v;
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        this.station = parent.getItemAtPosition(position).toString();





    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);






    }
/*
    public void addListenerOnButton(){

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                Log.d("tag","selectId" + selectedId);

                // find the radiobutton by returned id
                radioButton = (RadioButton) view.findViewById(selectedId);


                //String radiovalue = ((RadioButton) rootView.findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();

                //sharedPrefManager.addRadio(radioButton.getText().toString());



            }

        }); */





    public void onAdd(View view) {



        String date1 = date.getText().toString();


        try {
            String amount = liters.getText().toString();

             amount1 = Double.valueOf(amount);

            String prices = price.getText().toString();

             prices1 = Double.valueOf(prices);

            String kilometers = driven.getText().toString();

            kilometers1 = Double.valueOf(kilometers);

            String mpg = consumption.getText().toString();

           mpg1 = Double.valueOf(mpg);

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Täytä kaikki kentät", Toast.LENGTH_LONG).show();
        }





     /*   Toast.makeText(getActivity(), fuel, Toast.LENGTH_LONG).show(); */

        RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());



        MarkDown markDown = new MarkDown(date1, station, rb.getText().toString(), amount1, prices1, kilometers1, mpg1);

        MarkdownViewModel markdownViewModel = ViewModelProviders.of(this).get(MarkdownViewModel.class);

        markdownViewModel.insert(markDown);







    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void onDetach() {
        super.onDetach();
    }
}
