package com.example.androidprojektikevat2020;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.androidprojektikevat2020.Model.MarkDown;

import java.util.List;

public class MarkdownViewModel extends AndroidViewModel {

    private MarkDownRepository mRepository;
    private LiveData<List<MarkDown>> mAllMarkdowns;

    private LiveData currentOtherCurrency;

    public MarkdownViewModel(Application application) {
        super(application);
        mRepository = new MarkDownRepository(application);
        mAllMarkdowns = mRepository.getAllCurrencies();
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }

    LiveData<List<MarkDown>> getAllMarkdowns() {
        return mAllMarkdowns;
    }

    public void insert(MarkDown m) {
        mRepository.insert(m);
    }


}
