package com.example.androidprojektikevat2020.Model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "markdowns")

public class MarkDown {

    @PrimaryKey(autoGenerate = true)
    private int markdownID;

    @NonNull
    private String date;

    @NonNull
    private String station;

    @NonNull
    private String fuel;

    @NonNull
    private Double amount;

    @NonNull
    private Double price;

    @NonNull
    private Double driven;

    @NonNull
    private Double consumption;

    public MarkDown(@NonNull String date, @NonNull String station, @NonNull String fuel, @NonNull Double amount,@NonNull Double price, @NonNull Double driven, @NonNull Double consumption) {

        this.date = date;
        this.station = station;
        this.fuel = fuel;
        this.amount = amount;
        this.price = price;
        this.driven = driven;
        this.consumption = consumption;



    }

    public void setMarkdownID(int markdownID) {
        this.markdownID = markdownID;
    }

    public int getMarkdownID() {
        return markdownID;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getStation() {
        return station;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getFuel() {
        return fuel;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Double getAmount() {

        return amount;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

    public void setDriven(double driven) {
        this.driven = driven;
    }

    public Double getDriven() {
        return driven;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public Double getConsumption() {
        return consumption;
    }



    }


